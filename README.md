# Dotnet pub/sub example

## Build the publisher

```
docker build -t foo/pub pub
```

## Build the subscriber

```
docker build -t foo/sub sub
```

## Run the publisher

```
docker run -it --rm --name foo-pub --hostname foo-pub -v $(pwd)/pub:/app foo/pub dotnet run
```

## Run the subscriber(s)

```
docker run -it --rm --name foo-sub-a --link foo-pub -v $(pwd)/sub:/app foo/sub dotnet run TopicA foo-pub:12345
docker run -it --rm --name foo-sub-b --link foo-pub -v $(pwd)/sub:/app foo/sub dotnet run TopicB foo-pub:12345
docker run -it --rm --name foo-sub-all --link foo-pub -v $(pwd)/sub:/app foo/sub dotnet run All foo-pub:12345
```

